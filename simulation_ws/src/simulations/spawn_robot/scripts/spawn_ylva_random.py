#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import random
import subprocess

# Main Program
if __name__ == "__main__":
	# list of available position to spawn a robot in office map
	office_positions = [[0, 0],
						[3.5, 0],
						[7.4, 0],
						[12, 0],
						[17, 3.5],
						[12, 3.5],
						[9, 3.5],
						[6, 3.5],
						[3.5, 3.5],
						[0, 3.5],
						[3.5, 6.5],
						[6, 6.5],
						[10, 6.5],
						[13, 6.5],
						[17, 6.5],
						[3.5, 8.5],
						[0, 8.5],
						[0, 10],
						[3.5, 10],
						[10, 10],
						[13, 10],
						[17, 12],
						[17, 10],
						[6, 12],
						[3.5, 12],
						[0, 12]]
	
	pos = random.choice(office_positions)
	print(pos)
	subprocess.call(["roslaunch","ylva_description","spawn_ylva.launch", "x:="+str(pos[0]), "y:="+str(pos[1])])
	
