# ROS-docker-jaiio2019
Files for jaiio 2019 ros enviroment

## simulation\_ws
Here are the main scanning algorithms.
In /src folder we can find:
- control
  * teleop\_robot: script for manually control the robot.
- mapping\_algorithm: manual exploration algorithm
  * kalervo\_gmapping\_launch.launch: launch file for gmapping slam algorithm for kalervo robot.
  * kalervo\_hector.launch: launch file for hector slam algorithm.
  * ylva\_gmapping.launch
  * ylva\_hector.launch
- simulations: autonomous exploration algorithm
  * kalervo\_2dnav: navigation stack configure for kalervo robot.
  * kalervo\_frontier\_exploration:
  * spawn\_robot: scripts for random spawn a robot model in an enviroment.
  * kalervo\_description: kalervo robot model, config and spawn launch file.
  * my\_worlds: worlds files for test, and logged maps.
  * ylva\_description: ylva robot model, config and spawn launch file.

## hector\_slam\_slam\_ws
Files for hector slam algorithm.
In /src folder the most important files are:
  - hector\_mapping: The SLAM node.
  - hector\_geotiff: Saving of map and robot trajectory to geotiff images files.
  - hector\_trajectory\_server: Saving of tf based trajectories. 

## How to use

### Create docker container
In order to create a container from the Dockerfile, open a terminal and write:
```bash
cd /path/to/this/repo
docker build -t ros:jaiio2019 .
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --name ros_jaiio2019 ros:jaiio2019
```

### Config docker container
Inside the container, run for make the projects files:
```bash
cd /home/ROS-docker-jaiio2019/simulation_ws
sh config_ws.sh
cd /home/ROS-docker-jaiio2019/hector_slam_ws
sh config_ws.sh
```

### Launch a simulation
Open a terminal in your PC and run 
```bash
xhost +local:docker
```
Now, start the docker container with:
```bash
docker start -i ros_jaiio2019
```
Inside the docker container run:
```bash
roslaunch my_worlds office.launch gui:=true
```
Exec another terminal inside the docker container 
```bash
docker exec -it ros_jaiio2019 bash
```
and run:
```bash
roslaunch kalervo_description spawn_kalervo.launch x:=0 y:=0
```
to spawn a robot in (0,0) position, or:

```bash
rosrun spawn_robot spawn_kalervo_random.py
```
to spawn randomly a robot in the map.
```bash
rosrun teleop_robot teleop_robot.py
```
This will allow you control the robot, moving around the environment.
### Create a map with gmapping
Repeat the "Launch a simulation" steps and, in a new terminal inside the container, run:
```bash
roslaunch mapping_algorithm kalervo_gmapping.launch
```
This will open a new window with Rviz. Move the robot around to create a map of the enviroment.
To stop the simulation press ctrl+c in each of the terminal.

### Create a map with hector slam
Repeat the "Launch a simulation" steps and, in a new terminal inside the container, run:
```bash
roslaunch mapping_algorithm kalervo_hector.launch
```
This will open a new window with Rviz. Move the robot around to create a map of the enviroment.
To stop the simulation press ctrl+c in each of the terminal.

### Move the robot to a target location
Inside the docker container run:
```bash
roslaunch my_worlds office.launch gui:=false
```
Exec another terminal inside the docker container 
```bash
docker exec -it ros_jaiio2019 bash
```
and run:
```bash
roslaunch kalervo_2dnav kalervo_configuration.launch
```
this will open Rviz.
Now, exec another terminal inside the container
```bash
docker exec -it ros_jaiio2019 bash
```
and run:
```bash
roslaunch kalervo_2dnav move_base.launch
```
This will configure the navigation stack provided by ROS. In the Rviz window, press the "2D Nav Goal" button, and select the target
position and orientation of the robot. After a moment, the robot will start to move to the desired position, and
concurrently build the map. To stop the simulation press ctrl+c in each of the terminal.


### Explore a specified location with frontier exploration
You need 4 terminal attached to the container for this. Inside the docker container run:
```bash
roslaunch my_worlds office.launch gui:=false
```
Exec the second terminal inside the container 
```bash
docker exec -it ros_jaiio2019 bash
```
and run:
```bash
roslaunch kalervo_frontier_exploration kalervo_front_explor.launch
```
this will open Rviz.

Exec the third terminal inside the container, and run:
```bash
roslaunch kalervo_frontier_exploration kalervo_front_explor_algorithm.launch
```

Exec the last terminal inside the container, and run:
```bash
roslaunch kalervo_frontier_exploration move_base.launch
```
Now, in the Rviz window, press "2D Nav Goal" and select a target location, near to the robot, in order to initialize the
map server.  When the robot stop, you need to define a closed region to explore. Press "Publish Point" and select the
first corner of the region you want to explore. Press again "Publish Point" to define a new corner, and repeat until
you´re done. The last corner must match the first one, in order to generate a closed region. For last, after the region
become a red color, publish another point inside the region you define before. This last point will initialize the frontier exploration algorithm.
